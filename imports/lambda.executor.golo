module gaas.lambda.executor

import gololang.Errors

# === Java Imports ===
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

struct lambdaResult = {
	type, value
}

function executeConcurrentLambda = |lambda, parameters| {
  # TODO:
  # - check lambda type
  # - check parameter type
  return trying({
    let executor = Executors.newSingleThreadExecutor()
    let future = executor: submit(asInterfaceInstance(java.util.concurrent.Callable.class, {
      return lambda: code()(parameters)
    }))
    let res = future: get(lambda: timeout(), MILLISECONDS())
    # ecrire une même future avec le même timeout
    # et la join
    executor: shutdown()  

    return lambdaResult(
      type= lambda: type(),
      value= res
    )
  })
}
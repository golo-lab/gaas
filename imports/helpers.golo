module gaas.helpers

# === Helpers ===
function log = |txt, args...| -> println(java.text.MessageFormat.format(txt, args))

function sysEnv = |variableName| -> System.getenv(): get("PORT")



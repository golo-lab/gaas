module gaas.lambda

import gololang.Errors
import gololang.JSON

# === Structures ===
----
contentType could be text, json, html
----
struct lambda = {
	name, version, code, timeout, description, type
}

# === Functions ===

function addLambdaToMemory = |jsonStringFunctionRecord, lambdasMap, envEvaluations| {
	
	return	trying({
			let data = JSON.parse(jsonStringFunctionRecord)
			let name = data: get("name")
			let description = data: get("description")
			let version = data: get("version")
			let timeout = Long.valueOf(data: get("timeout")) # Anything to Long
			let source = data: get("source")
			let type = data: get("type")

			let lambdaKey = "gaas:"+name+"-"+version
			# add to lambdasMap
			lambdasMap: put(lambdaKey, 
				lambda(
						name= name,
						description= description,
						version= version,
						timeout= timeout,
						code= envEvaluations: def(source),
						type= type
					)			
			)
			return lambdaKey
		})
}
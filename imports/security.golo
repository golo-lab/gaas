module gaas.security

import gololang.Adapters

function getSecurityManagerDefinition = {
  let securityManagerDefinition = Adapter()
    : extends("java.lang.SecurityManager")
    : overrides("checkExit", |super, this, status| {
        #super(this)
        throw java.lang.SecurityException("😡 exit is ⛔️")
    }) 
    : overrides("checkDelete", |super, this, file| {
        #super(this)
        throw java.lang.SecurityException("😡 delete is ⛔️")
    }) 
    : overrides("checkExec", |super, this, cmd| {
        #super(this)
        throw java.lang.SecurityException("😡 exec is ⛔️")
    })
    : overrides("checkWrite", |super, this, file| {
        #super(this)
        #println(file)
        throw java.lang.SecurityException("😡 write is ⛔️")
    })
    return securityManagerDefinition
}

----
Made with ❤️ with Vert-x and Golo
Author: @k33g_org on Twitter
----
module gaas

# TODO
# - Pub / Sub to publish change between GaaS instances
# - Log or metrics to Prometheus
# - create a CLI to deploy from files (or use curl)
# - discovery

# === Golo Imports ===
# TO CHECK: I think this import is not needed, because it's defined in ./imports/security.golo
# but I get an error if I ommit it here:
# `java.lang.NoSuchMethodError: class gololang.Adapters.types.adapter::newInstance`
import gololang.Adapters
import gololang.Errors
import gololang.JSON

# === Vert-x Imports ===
import io.vertx.core.Vertx
import io.vertx.core.http.HttpServer
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler

import io.vertx.redis.RedisOptions
import io.vertx.redis.RedisClient

# === Project Imports ===
import gaas.security
import gaas.helpers
import gaas.lambda
import gaas.lambda.executor

# === Variables, Objects, ... ====
let lambdasMap = map[]
let envEvaluations = gololang.EvaluationEnvironment()

let vertx = Vertx.vertx()


# === Redis Part ===
function getRedisClient = |vertx| {
	
	return trying({
		let redisPort = Integer.parseInt(sysEnv("REDIS_PORT") orIfNull "6379")
		let redisHost = sysEnv("REDIS_HOST") orIfNull "127.0.0.1"
		let redisAuth = sysEnv("REDIS_PASSWORD") orIfNull null # LOL

		let config = RedisOptions()
									: setHost(redisHost)
									: setPort(redisPort)
									: setAuth(redisAuth)
		let redis = RedisClient.create(vertx, config)
		return redis
	})

}

function main = |args| {

	# --- security ---
  let securityManagerDefinition = getSecurityManagerDefinition()
  let securityManager = securityManagerDefinition: newInstance()  
  System.setSecurityManager(securityManager)
	# --- end security ---

	# --- redis ---
	let redisCli = getRedisClient(vertx): either(
		|client| { # 👋 if I name returnResult result 💥
			log("🙂 redis client creation {0}", client)
			return client
		},
		|error| {
			log("😡 redis client creation {0}", error)
			# TODO throw error and stop the application
		}
	)

	# load and compile function when starting
	redisCli: keys("gaas:*", |ar| {
		if ar: succeeded() {
			log("🙂 commandGetkeys {0}", ar: result())
			ar: result(): each(|key| {
				log("👋 key {0}", key)
				# add to memory and compile
				redisCli: get(key , |res| {
					if res: succeeded() {
						log("🙂 function found {0}", key)

						#log("🙂 function found {0}", res: result())

						# add the lambda to memory + the compiled function
						addLambdaToMemory(res: result(), lambdasMap, envEvaluations): either(
							recover= |error| -> log("😡 function not compiled {0}", error),
							mapping= |returnResult| -> log("🙂 function compiled {0}", returnResult)
						)
					} else { # res failed
						log("😡 function not compiled {0}", res: cause())
					}
				})
			})
		} else { # ar failed
			log("😡 commandGetkeys {0}", ar: cause())
			# TODO manage errors
		}
	})

	# --- end redis ---

	# --- http server ---
	let server = vertx: createHttpServer()
  let router = Router.router(vertx)
  router: route(): handler(BodyHandler.create())
  let port =  Integer.parseInt(sysEnv("PORT") orIfNull "9090")

	# +++ deploy a function +++
  router: post("/deploy"): handler(|context| {
    context: response(): putHeader("content-type", "application/json;charset=UTF-8")

		let payload = context: getBodyAsString()

		# add the lambda to memory + the compiled function
		addLambdaToMemory(payload, lambdasMap, envEvaluations): either(
      recover= |error| {
        log("😡 with deploy {0}", error)
        context: response(): end(JSON.stringify(DynamicObject(): result(error: message())), "UTF-8")
      },
			mapping= |lambdaKey| { # 👋 if I name returnResult result 💥
				# function persistence
				let redisKey = lambdaKey
				redisCli: set(redisKey, payload , |res| {
					if res: succeeded() {
						log("🙂 function persisted {0}", redisKey)
					} else {
						log("😡 function not persisted {0}", res: cause())
					}
				})

				log("🙂 with deploy {0}", lambdaKey)
				context: response(): end(JSON.stringify(DynamicObject(): result(lambdaKey)), "UTF-8")
      }
    )
  })

	# +++ End deploy a function +++

	# +++ execute a function +++

	let returnAdequateError = |context, error| {
		# TODO test null on contentType
		let contentType = context: response(): headers(): get("content-type")
		log("🦊 type: {0}", contentType)
		match {
			when contentType: startsWith("application/json;") then context: response(): end(JSON.stringify(DynamicObject(): error(error: message() orIfNull "😡 I'm not sure, but you probably had a timeout")), "UTF-8")
			when contentType: startsWith("text/plain;") then context: response(): end(error: message(), "UTF-8")
			when contentType: startsWith("text/html;") then context: response(): end(error: message(), "UTF-8")
			otherwise context: response(): end(error: message(), "UTF-8")
		}
	}

	let returnAdequateContent = |context, returnResult| {
		# TODO test null on contentType
		let contentType = context: response(): headers(): get("content-type")
		log("🦊 type: {0}", contentType) 
		match {
			when contentType: startsWith("application/json;") then context: response(): end(JSON.stringify(DynamicObject(): result(returnResult: value())), "UTF-8")
			when contentType: startsWith("text/plain;") then context: response(): end(returnResult: value(), "UTF-8")
			when contentType: startsWith("text/html;") then context: response(): end(returnResult: value(), "UTF-8")
			otherwise context: response(): end(returnResult: value(), "UTF-8")
		}
	}

	let setReturnContentType = |context, lambda| {
		log("🐼 type: {0}", lambda: type())
		match {
			when lambda: type(): equals("json") then context: response(): putHeader("content-type", "application/json;charset=UTF-8")
			when lambda: type(): equals("text") then context: response(): putHeader("content-type", "text/plain;charset=UTF-8")
			when lambda: type(): equals("html") then context: response(): putHeader("content-type", "text/html;charset=UTF-8")
			otherwise context: response(): putHeader("content-type", "text/plain;charset=UTF-8")
		}		
	}

	let getLambdaFromMemory = |name, version| {
		let lambdaKey = "gaas:"+name+"-"+version
		let lambda = lambdasMap: get(lambdaKey) # from memory
		return lambda
	}

	let runLambda = |lambda, parameters| {
		let result = executeConcurrentLambda(lambda, parameters)
					: either(
						recover= |error| {
							log("😡 with computation {0}", error)
							throw error 
							#return error: message() # TODO  or 👋 throw error and return error
						},
						mapping= |computationResult| {
							log("🙂 currently all is ok, result is: {0}", computationResult)
							return computationResult
						}
					)
		log("🤖 result: {0}", result)
		return result # type lambdaResult(type, value)		
	}

  router: post("/run"): handler(|context| {
    #context: response(): putHeader("content-type", "application/json;charset=UTF-8")

    trying({
      let data = JSON.parse(context: getBodyAsString())

			log("🙂 with run {0}", data)
      
			let name = data: get("name")
			let version = data: get("version")

			# To pass parameters to the function and call it:

			# fetch(`http://localhost:9090/run`, {
			# 	method: "POST",
			# 	body: JSON.stringify({
			# 		name: "hi",
			# 		version: "1.0.0",
			# 		parameters: {
			# 			message: "Bob Morane"
			# 		}
			# 	})
			# })			

			let parameters = JSON.toDynamicObjectFromMap(data: get("parameters"))
			let lambda = getLambdaFromMemory(name, version)
			# get return type of the lambda and set the return content-type
			setReturnContentType(context, lambda)

			return runLambda(lambda, parameters)
    })
    : either(
				recover= |error| {
					log("😡 >> with run {0}", error)
					returnAdequateError(context, error)
					# I need to trap the timeout the exception
				},
				mapping= |returnResult| { # 👋 if I name returnResult result 💥
					log("🙂 with run {0}", returnResult)
					returnAdequateContent(context, returnResult)
				}			
    )
  })

	let executeGetLambdaRequest = |context| {
		trying({

			let name = context: request(): getParam("function_name")
			let version = context: request(): getParam("function_version")
			let string_parameters = context: request(): getParam("function_parameters") orIfNull "{}"
			#log("0- 🎃 with GET run {0}", string_parameters)
			log("1- 🎃 with GET run {0}", name+"/"+version+"/"+string_parameters)
			log("2- 🎃 with GET run {0}", JSON.parse(string_parameters))
			let parameters = JSON.toDynamicObjectFromMap(JSON.parse(string_parameters))
			log("3- 🎃 with GET run {0}", parameters)

			let lambda = getLambdaFromMemory(name, version)
			# get return type of the lambda and set the return content-type
			setReturnContentType(context, lambda)
			return runLambda(lambda, parameters)

		})
    : either(
				recover= |error| {
					log("😡 >> with run {0}", error)
					returnAdequateError(context, error)
					# I need to trap the timeout the exception
				},
				mapping= |returnResult| { # 👋 if I name returnResult result 💥
					log("🙂 with run {0}", returnResult)
					returnAdequateContent(context, returnResult)
				}			
    )
	}
	
	# http://localhost:9090/run/home/1.0.0
	router: get("/run/:function_name/:function_version"): handler(|context| {
		executeGetLambdaRequest(context)
	})
	# http://localhost:9090/run/add/1.0.0/{"a":30,"b":12}
	# http://localhost:9090/run/home/1.0.0/{}
	# http://localhost:9090/run/home/1.0.1/{"message":"propulsed with Golo"}
	router: get("/run/:function_name/:function_version/:function_parameters"): handler(|context| {
		executeGetLambdaRequest(context)
	})

	# +++ End execute a function +++

	# Static assets: see ./webroot directory
  router: route("/*"): handler(StaticHandler.create())
  server: requestHandler(|httpRequest| -> router: accept(httpRequest)): listen(port)
  println("🌍 GaaS server listening on " + port)

	# --- end http server ---

}
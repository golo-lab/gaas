const fetch = require('node-fetch');


fetch(`http://localhost:9090/run`, {
  method: "POST",
  body: JSON.stringify({
    name: "hello",
    version: "1.0.0"
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/run`, {
  method: "POST",
  body: JSON.stringify({
    name: "hi",
    version: "1.0.0",
    parameters: {
      message: "Bob Morane"
    }
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

// GET way http://localhost:9090/run/add/1.0.0/{"a":30,"b":12}

fetch(`http://localhost:9090/run`, {
  method: "POST",
  body: JSON.stringify({
    name: "add",
    version: "1.0.0",
    parameters: {
      a: 30, b: 20
    }
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/run`, {
  method: "POST",
  body: JSON.stringify({
    name: "listFiles",
    version: "1.0.0",
    parameters: {}
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/run`, {
  method: "POST",
  body: JSON.stringify({
    name: "exit",
    version: "1.0.0",
    parameters: {}
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/run`, {
  method: "POST",
  body: JSON.stringify({
    name: "neverEnd",
    version: "1.0.0",
    parameters: {}
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))

fetch(`http://localhost:9090/run`, {
  method: "POST",
  body: JSON.stringify({
    name: "divide",
    version: "1.0.0",
    parameters: {}
  })
})
.then(res => res.json())
.then(res => console.log(res))
.catch(err => console.log(err))